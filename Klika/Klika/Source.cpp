#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <stdlib.h>
#include <stdio.h>


using namespace std;

struct Lista {
	Lista *next;
	int cel;
};
Lista **tabwierz;
void Brosh(Lista *Wierzcholki, int Wynik, int poparcie, bool *wygrana) {
	if (Wynik >= poparcie)
		*wygrana = 1;

	if (*wygrana != true) {
		//pivot
		Lista *Head = Wierzcholki;
		Lista *tmp = NULL;
		Lista *tmp2 = Wierzcholki;
		int pivot = -1;
		int maxs = 0;
		int sasiad = 0;
		while (Head != NULL) {
			sasiad = 0;
			tmp = tabwierz[Head->cel];
			while (tmp != NULL) {
				tmp2 = Wierzcholki;
				while (tmp2 != NULL) {
					if (tmp->cel == tmp2->cel) {
						sasiad++;
					}
					tmp2 = tmp2->next;
				}

				tmp = tmp->next;
			}
			if (sasiad >= maxs) {
				maxs = sasiad;
				pivot = Head->cel;
			}
			Head = Head->next;
		}
		Head = NULL;
		if (pivot != -1) {
			tmp = Wierzcholki;
			while (tmp != NULL) {
				tmp2 = tabwierz[pivot];
				bool flag = 0, flag2 = 0;
				while (tmp2 != NULL) {
					if (tmp->cel == tmp2->cel) {
						flag = 1;
						break;
					}
					tmp2 = tmp2->next;
				}
				if (flag == 0) {
					if (Head == NULL) {
						Head = new Lista;
						Head->cel = tmp->cel;
						Head->next = NULL;
						break;
					}
					else {
						tmp2 = NULL;
						tmp2 = new Lista;
						tmp2->cel = tmp->cel;
						tmp2->next = Head;
						Head = tmp2;
						break;
					}
				}
				tmp = tmp->next;
			}


			if (Head == NULL) {
				Head = new Lista;
				Head->cel = pivot;
				Head->next = NULL;
			}
			else {
				tmp2 = NULL;
				tmp2 = new Lista;
				tmp2->cel = pivot;
				tmp2->next = Head;
				Head = tmp2;
			}
		}
		tmp = NULL;
		while (Head != NULL && *wygrana != true) {
			Wynik++;
			if (Wynik >= poparcie) {
				*wygrana = 1;
				break;
			}
			int pomoc = 0;
			Lista *WierzchDalej = NULL;
			Lista *Tmp = tabwierz[Head->cel];
			while (Tmp != NULL) {
				Lista *Tmp2 = Wierzcholki;
				while (Tmp2 != NULL) {
					if (Tmp->cel == Tmp2->cel) {
						if (WierzchDalej == NULL) {
							WierzchDalej = new Lista;
							WierzchDalej->cel = Tmp->cel;
							WierzchDalej->next = NULL;
							pomoc++;
							break;
						}
						else {
							tmp = new Lista;
							tmp->cel = Tmp->cel;
							tmp->next = WierzchDalej;
							WierzchDalej = tmp;
							pomoc++;
							break;
						}
					}
					Tmp2 = Tmp2->next;
				}
				Tmp = Tmp->next;
			}
			if (pomoc + Wynik >= poparcie)
				Brosh(WierzchDalej, Wynik, poparcie, wygrana);

			Wynik--;
			Head = Head->next;
		}
	}
}

int main() {
	for (int hue = 0; hue < 100; hue++) {
		int posly, poslygrupa = 0, komisje, poparcie;
		bool wygrana = 0;
		Lista *wierz, *TMP, *Head = NULL;
		cin >> posly;
		cin >> komisje;
		cin >> poparcie;

		tabwierz = new Lista*[posly];
		int **tab = new int*[posly];
		for (int x = 0; x < posly; x++) {
			tabwierz[x] = NULL;
			tab[x] = new int[posly];
		}

		for (int i = 0; i < posly; i++) {
			for (int j = 0; j < posly; j++) {
				scanf("%d", &tab[i][j]);
				if (tab[i][j] == 1) {
					wierz = new Lista;
					wierz->cel = j;
					wierz->next = tabwierz[i];
					tabwierz[i] = wierz;
				}
			}
		}
		if (posly == 1 && poparcie == 1) {
			printf("Tak\n");
			continue;
		}
		else if (posly < poparcie) {
			printf("Nie\n");
			continue;
		}
		for (int k = 0; k < posly; k++) {
			if (tabwierz[k] != NULL) {
				if (Head == NULL) {
					Head = new Lista;
					Head->cel = k;
					Head->next = NULL;
					poslygrupa++;
				}
				else {
					wierz = new Lista;
					wierz->cel = k;
					wierz->next = Head;
					Head = wierz;
					poslygrupa++;
				}
			}
		}

		int Wynik = 0;
		if (poslygrupa >= poparcie && Head != NULL)
			Brosh(Head, Wynik, poparcie, &wygrana);

		if (wygrana != 1) {
			for (int y = 2; y < komisje + 1; y++) {



				for (int v = 0; v < posly; v++)
				{
					wierz = tabwierz[v];
					while (wierz != NULL)
					{
						TMP = wierz;
						wierz = wierz->next;
						delete TMP;
					}
				}

				for (int x = 0; x < posly; x++) {
					tabwierz[x] = NULL;
				}

				for (int i = 0; i < posly; i++) {
					for (int j = 0; j < posly; j++) {
						if (tab[i][j] == y) {
							wierz = new Lista;
							wierz->cel = j;
							wierz->next = tabwierz[i];
							tabwierz[i] = wierz;
						}
					}
				}

				Head = NULL;
				poslygrupa = 0;

				for (int k = 0; k < posly; k++) {
					if (tabwierz[k] != NULL) {
						if (Head == NULL) {
							Head = new Lista;
							Head->cel = k;
							Head->next = NULL;
							poslygrupa++;
						}
						else {
							wierz = new Lista;
							wierz->cel = k;
							wierz->next = Head;
							Head = wierz;
							poslygrupa++;
						}
					}
				}


				if (poslygrupa >= poparcie && Head != NULL)
					Brosh(Head, Wynik, poparcie, &wygrana);

			}
		}
		if (wygrana == 1)
			printf("Tak");
		else
			printf("Nie");
		printf("\n");
	}
	getchar();
	getchar();
	return 0;
}

/*
Nasta�y ci�kie czasy.W d�ungli szaleje w�ciek�y zwierz Kryzys.Tarzan ju� prawie straci� nadziej�(jak dobrze pami�tamy chcia� zosta� bananowym baronem).Niestety w jego serce wkrad� si� smutek, Siedmiokropka opanowa�a sprzeda� i dystrybucj� banan�w na dobre, a buszmeni atakuj� niewielkimi grupkami jej kasy(codziennie rano).Co wi�cej, jej prawnicy(w liczbie 2000), wykorzystuj�c najskrytsze zakamarki KKSu, unicestwili konkurencj�.Tarzan musi walczy� o honor, rodzin�, oczywi�cie Jaguara oraz mniej wznios�e sprawy, tzn.regularn� sp�at� kredytu.Pragnie zmieni� swoj� d�ungl� i zosta� politykiem(poprzez wybory uzupe�niaj�ce).W jednym z wywiad�w zdenerwowany wykrzycza� : "Welcome to the jungle Siedmiokropka!".Tarzan jest zdeterminowany, podejmie pr�b� zmiany prawa.W tym celu b�dzie musia� przekona� do swoich racji cz�� polityk�w.

Pomi�dzy politykami w najwy�szej Izbie Buszrlamentu NIB panuj� dziwne zwyczaje(doprawdy : ).Ka�dy polityk uczestniczy w co najmniej jednej podkomisji finans�w, a dowolnych dw�ch polityk�w zna si� z dok�adnie jednej podkomisji(istniej� politycy z jednej podkomisji, kt�rzy si� nie znaj�).Co ciekawe grupa polityk�w, kt�rzy wzajemnie znaj� si� z jednej podkomisji wspiera si� w g�osowaniach.Tarzan chce mie� realn� w�adz�, dlatego szuka jak najwi�kszej takiej grupy, aby przekona� j� do zmian.Czy w NIB jest co najmniej p pos��w wspieraj�cych si�(wzajemnie znaj�cych si� z jednej podkomisji) ? Czy Tarzan osi�gnie sw�j cel ?

n - liczba wszystkich polityk�w(n<200).
	k - liczba podkomisji finans�w.
	p - liczba "wspieraj�cych si�" polityk�w, kt�rych nale�y przekona�.
	Powi�zania polityk�w w podkomisjach finans�w zapisane s� w postaci macierzy s�siedztwa A.Element A(i, j) okre�la podkomisj� finans�w, z kt�rej znaj� si� pos�owie.Elementy diagonalne macierzy A ustawione s� na 0, pozosta�e s� liczbami ca�kowitymi z przedzia�u[1, k].



	Wej�cie
	5 2 3
	0 1 1 1 1 1 0 2 2 1 1 2 0 2 2 1 2 2 0 1 1 1 2 1 0
	5 2 4
	0 1 1 1 1 1 0 2 2 1 1 2 0 2 2 1 2 2 0 1 1 1 2 1 0
	6 3 3
	0 1 3 2 1 1 1 0 2 1 2 2 3 2 0 2 2 2 2 1 2 0 2 2 1 2 2 2 0 2 1 2 2 2 2 0

	Wyj�cie
	Tak
	Nie
	Tak
	*/