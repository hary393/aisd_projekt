#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <stdio.h>
#include <cmath>
using namespace std;
void permute(int a[20], int i, int n, double *wynik, double *wynikpom, int wioski[20][2])
{
	int j;
	bool flag = 0;

	double brake = 0;
	*wynikpom = 0;
	for (int x = 0; x < n + 1; x++) {
		double tmp = 0, tmp1 = 0, tmp2 = 0;
		int perm = a[x];
		int perm2 = a[x + 1];
		tmp = (wioski[perm][0] - wioski[perm2][0]);
		tmp1 = (wioski[perm][1] - wioski[perm2][1]);
		tmp2 = tmp*tmp + tmp1*tmp1;
		tmp2 = sqrt(tmp2);
		if (tmp2>1000 && (wioski[perm][1] == wioski[perm2][1])) {
			tmp2 = wioski[perm][0] - wioski[perm2][0];
			if (tmp2<0) {
				tmp2 = tmp2*-1;
			}
			*wynikpom += tmp2;
		}
		else {
			if (tmp2>1000) {
				if (tmp1 < 0) {
					tmp1 = tmp1*-1;
				}
				tmp2 = (wioski[perm][0] + wioski[perm2][0]) + tmp1;
				*wynikpom += tmp2;
			}
			else {
				*wynikpom += tmp2;
			}
		}
		if (x < i) {
			brake += tmp2;
			if (brake>*wynik) {
				flag = 1;
				break;
			}
		}
	}
	if (*wynikpom < *wynik) {
		*wynik = *wynikpom;
	}
	for (j = i; j <= n; j++) {
		if (flag == 1) {
			break;
		}
		swap((a[i]), (a[j]));
		permute(a, i + 1, n, wynik, wynikpom, wioski);
		swap((a[i]), (a[j]));
	}

}
int main() {
	int wioski[20][2];


	int liczbatestow = 0;
	scanf("%i", &liczbatestow);
	for (int c = 0; c < liczbatestow; c++) {
		int dane[20][2] = { 0 };
		int wioski[20][2] = { 0 };
		double wynik = 100000;
		double wynikpom = 0;
		int hue;
		char tmp;
		int tmp2;

		scanf("%i", &hue);
		scanf("%c", &tmp);
		scanf("%c", &tmp);

		if (hue != 1) {
			for (int jw = 0; jw < hue; jw++) {

				scanf("%c", &tmp);
				scanf("%i", &tmp2);
				dane[jw][0] = tmp2;
				scanf("%c", &tmp);
				scanf("%i", &tmp2);
				dane[jw][1] = tmp2;
				scanf("%c", &tmp);
				scanf("%c", &tmp);
			}
			for (int i = 0; i < hue; i++) {
				for (int j = 1; j <= hue; j++) {
					if (dane[i][0] == dane[j][0] && dane[i][0] != -123) {
						if (dane[i][1] == dane[j][1] && i != j)
							dane[j][0] = -123;
					}
				}
			}
			int kk = 0;
			for (int h = 0; h < hue; h++) {
				if (dane[h][0] != -123) {
					wioski[kk][0] = dane[h][0];
					wioski[kk][1] = dane[h][1];
					kk++;
				}
			}
			int a[21] = { 0 };
			for (int x = 0; x < kk; x++) {
				a[x] = x;
			}
			permute(a, 1, kk - 1, &wynik, &wynikpom, wioski);
			int lask = wynik;
			printf("%i", lask);
			printf("\n");
		}
		else {
			printf("0");
			printf("\n");
		}
	}
	getchar();
	getchar();
	getchar();
	return 0;

}
/*
Tarzan nie straszy ju� dzieci tubylc�w.
Odk�d si� zakocha�, postanowi�, �e b�dzie prowadzi� uczciw� dzia�alno�� gospodarcz�.
Teraz Tarzan rozprowadza banany po d�ungli.Wi�kszo�� dr�g jest mu znana i prowadzi
najkr�tsz� drog� pomi�dzy dwoma wioskami(plemionami).Jednak Tarzan nie pami�ta(nie zna)
dr�g, kt�rych d�ugo�� wynosi co najmniej 1000 metr�w.Je�li Tarzan nie zna
drogi z wioski A do wioski B, to woli przej�� si� najkr�tsz� drog� do rzeki(opisanej prost� x = 0),
a nast�pnie wzd�u� rzeki do punktu, w kt�rym nast�pna wioska, kt�r� odwiedzi,
b�dzie najbli�ej rzeki.Z tego punktu kieruje si� ju� prosto, najkr�tsz� drog� do
wspomnianej wioski(chyba, �e wioska jest po drodze, to nie musi ju� jej odwiedza�).
Tarzan pragnie zakupi� Jaguara, a�eby powi�kszy� interes i zaimponowa� ukochanej.
Pom� mu!Jaka b�dzie najkr�tsza droga S, zgodna z powy�szymi warunkami, prowadz�ca z
dowolnej wioski W, przechodz�ca przez wszystkie
pozosta�e wioski i ko�cz�ca si� w wiosce W ?
Wej�cie
2
4 ((112, 507), (638, 639), (335, 159), (692, 591))
5 ((82, 18), (575, 211), (433, 1), (318, 5), (521, 391))

Wyj�cie
1718
1179
*/





