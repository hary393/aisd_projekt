#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <stdio.h>
#include <iostream>

using namespace std;
struct Lista
{
	Lista * next;
	int cel;
	int waga;
	int f;
};

int main()
{
	int  m, n, u, x, y, *droga, *poprz;
	bool *odwiedzone;
	Lista **graf;
	Lista *lista;
	short int szerokosc, wysokosc;
	short int poczx, poczy;
	int pocz;
	short int koniecx, koniecy;
	int koniec;
	int wyciag;
	int waga, tmp, i, j;
	int hue, czas;
	scanf("%hi %hi", &szerokosc, &wysokosc);
	int **tab = new int *[szerokosc];
	for (int z = 0; z < szerokosc; z++) {
		tab[z] = new int[wysokosc];
	}
	n = szerokosc*wysokosc;
	m = (szerokosc - 1) * (wysokosc - 1) * 2;
	scanf("%hi %hi", &poczx, &poczy);
	pocz = poczx + szerokosc * poczy;
	scanf("%hi %hi", &koniecx, &koniecy);
	koniec = koniecx + szerokosc*koniecy;

	droga = new int[n];
	poprz = new int[n];
	odwiedzone = new bool[n];
	graf = new Lista *[n];


	for (int i = 0; i < n; i++)
	{
		droga[i] = 800000;
		poprz[i] = -1;
		odwiedzone[i] = false;
		graf[i] = NULL;
	}


	scanf("%i", &wyciag);
	short int a, b, f;
	for (int n = 0; n < wyciag; n++) {
		scanf("%hi %hi", &a, &b);
		x = a + b*szerokosc;
		scanf("%hi %hi", &a, &b);
		y = a + b*szerokosc;
		scanf("%i", &waga);
		scanf("%hi", &f);
		lista = new Lista;
		lista->cel = y;
		lista->waga = waga;
		lista->f = f;
		lista->next = graf[x];
		graf[x] = lista;

	}
	for (short int ff = 0; ff < wysokosc; ff++) {
		for (short int kk = 0; kk < szerokosc; kk++) {
			scanf("%hi", &tmp);
			tab[ff][kk] = tmp;
		}
	}


	for (i = 0; i<wysokosc; i++) {
		for (j = 0; j<szerokosc; j++) {
			x = j + i*szerokosc;
			if (i - 1 >= 0) {
				y = j + (i - 1)*szerokosc;
				if (tab[i][j] >= tab[i - 1][j])
					waga = 1;
				else
					waga = tab[i - 1][j] - tab[i][j] + 1;
				lista = new Lista;
				lista->cel = y;
				lista->waga = waga;
				lista->f = 1;
				lista->next = graf[x];
				graf[x] = lista;
			}
			if (i + 1 < wysokosc) {
				y = j + (i + 1)*szerokosc;
				if (tab[i][j] >= tab[i + 1][j])
					waga = 1;
				else
					waga = tab[i + 1][j] - tab[i][j] + 1;
				lista = new Lista;
				lista->cel = y;
				lista->waga = waga;
				lista->f = 1;
				lista->next = graf[x];
				graf[x] = lista;
			}
			if (j - 1 >= 0) {
				y = j - 1 + i*szerokosc;
				if (tab[i][j] >= tab[i][j - 1])
					waga = 1;
				else
					waga = tab[i][j - 1] - tab[i][j] + 1;
				lista = new Lista;
				lista->cel = y;
				lista->waga = waga;
				lista->f = 1;
				lista->next = graf[x];
				graf[x] = lista;
			}
			if (j + 1 < szerokosc) {
				y = j + 1 + i*szerokosc;
				if (tab[i][j] >= tab[i][j + 1])
					waga = 1;
				else
					waga = tab[i][j + 1] - tab[i][j] + 1;
				lista = new Lista;
				lista->cel = y;
				lista->waga = waga;
				lista->f = 1;
				lista->next = graf[x];
				graf[x] = lista;
			}
		}
	}

	cout << endl;

	droga[pocz] = 0;

	for (i = 0; i < n; i++)
	{

		for (j = 0; odwiedzone[j]; j++);
		for (u = j++; j < n; j++)
			if (!odwiedzone[j] && (droga[j] < droga[u]))
				u = j;

		odwiedzone[u] = true;

		for (lista = graf[u]; lista; lista = lista->next)
			if (!odwiedzone[lista->cel] && (droga[lista->cel] > droga[u] + lista->waga))
			{
				hue = droga[u] % lista->f;
				if (hue == 0) {
					czas = lista->waga;
				}
				else czas = lista->f - (droga[u] % lista->f) + lista->waga;

				droga[lista->cel] = droga[u] + czas;
				poprz[lista->cel] = u;
			}
	}


	cout << droga[koniec] << endl;

	Lista *Tmp2;
	for (i = 0; i < n; i++)
	{
		lista = graf[i];
		while (lista)
		{
			Tmp2 = lista;
			lista = lista->next;
			delete Tmp2;
		}
	}

	delete[] droga;
	delete[] poprz;
	delete[] odwiedzone;
	delete[] graf;
	return 0;
}

/*

Spacer po g�rach
Celem zadania jest znalezienie najkr�tszej trasy przez dany teren.Teren podzielony jest na pola.Ka�de pole ma pewn� wysoko��, wyra�on� nieujemn� liczb� ca�kowit�.Przej�cie na pole o wysoko�ci A z pola o wysoko�ci B zajmuje :
A - B + 1 minut, je�eli A > B,
1 minut�, je�eli A ? B.
Mo�emy przechodzi� tylko na pola s�siaduj�ce ze sob� jednym z bok�w, czyli z danego pola mo�emy przej�� na co najwy�ej cztery s�siednie.Nie mo�emy opu�ci� terenu opisanego przez map�.

W obszarze mo�e znajdowa� si� pewna liczba wyci�g�w.Wyci�g umo�liwia dotarcie z jego punktu startowego wyci�gu do punktu docelowego wyci�gu(jest jednokierunkowy).Skorzystanie z wyci�gu zajmuje pewn� liczb� minut.Dodatkowo, wyci�gi kursuj� w okre�lonych minutach -- je�eli znajdujemy si� w polu startowym wyci�gu w minucie 8 i wiemy, �e punktem docelowym wyci�gu jest(12, 12), kursuje on co 5 minut i skorzystanie z niego zajmuje 3 minuty, to w punkcie(12, 12)
b�dziemy w 13 minucie(13 = 8 + 2 + 3; 2 minuty oczekiwania plus trzy minuty jazdy).

Wej�cie
9 8 0 0 8 7 0
0 0 0 0 0 0 0 0 8
0 9 9 9 9 9 9 9 0
0 1 0 1 0 0 0 9 0
0 9 0 0 0 1 0 9 0
0 9 1 1 1 1 0 9 0
0 9 0 0 0 0 0 9 0
0 9 9 9 9 1 9 9 0
8 0 0 0 0 0 0 0 0


Wyj�cie
18

Wej�cie
9 9 0 0 8 8 5
0 1 2 0 5 2
0 1 2 0 1 3
2 0 4 0 3 7
4 1 8 8 20 11
4 2 8 8 10 12
0 9 0 9 0 9 0 9 0
0 9 0 9 0 9 0 9 0
0 9 0 9 0 9 0 9 0
0 9 0 9 0 9 0 9 0
0 9 0 9 0 9 0 9 0
0 9 0 9 0 9 0 9 0
0 9 0 9 0 9 0 9 0
0 9 0 9 0 9 0 9 0
0 9 0 9 0 9 0 9 0

Wyj�cie
22*/